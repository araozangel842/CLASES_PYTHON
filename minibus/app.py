# Python POO - MINIBUS
# Def CLASES
class Vehiculo():
     def __init__(self, tipo):
          self.tipo = tipo
     
     def __str__(self):
          return f"{self.tipo}"

class Minibus(Vehiculo):

     def __init__(self, capacity = 6):
          Vehiculo.__init__(self, "Minibus")
          self.capacity = capacity
          self.passengers = []

     def free_places(self):
          return self.capacity-len(self.passengers)

     def take_passengers(self, Passenger):
          if not self.free_places():
               return False
          self.passengers.append(Passenger)
          return True

     def __str__(self):
          return f"Vehículo Minibus of capacity {self.capacity}"
     
class Passenger():
     def __init__(self,dni, name):
          self.dni = dni
          self.name = name

     def __str__(self):
          return f"Person: DNI {self.dni} ----Name: {self.name}"

### Funciones adicionales
def find_person(person, dni):
     for one_person in person:
          if one_person.dni == dni:
               return True
     return False

def get_person(person):
     while len(person) < 40:
          try:
               dni = int(input("Ingresa el DNI de la Persona (0 para terminar)"))
               if dni == 0: break
               if find_person(person, dni):
                    print("Person is already inside. Try again")
               else:
                    name = input("Give me name: ")
                    if name:
                         person_instance = Passenger(dni, name)
                         person.append(person_instance)
                    else:
                         print("There is no name. Try again.")
          except ValueError:
               print("Wrong value for DNI. Try again.")
  



### Aplicación

print("Starting app")
minibus1 = Minibus(2)

list_person = []

get_person(list_person)

for p in list_person:
     if minibus1.get_person(p):
          print("Passenger inside {p} to {minibus1}")
     else:
          print("THe bus is already full {minibus1} and cant come {p}")
print("End of all passengers")